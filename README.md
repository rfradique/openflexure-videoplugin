# video-extension

A video recording extension for the OpenFlexure Microscope.

To use, download the extension and save it in `/var/openflexure/extensions/microscope_extensions` on your microscope. (You may need to adjust your user permissions.)

For more information about extensions, see the [API documentation](https://openflexure-microscope-software.readthedocs.io/en/master/plugins.html) and the [handbook](https://openflexure.gitlab.io/microscope-handbook/develop-extensions/index.html).
